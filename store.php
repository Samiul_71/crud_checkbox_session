<?php
require_once('lib/app.php');

$id = $_GET['id'];
$user = array();
$user['full_name'] = $_POST['full_name'];
$user['hobbies'] = $_POST['hobbies'];

if(!isset($_SESSION['users']) AND empty($_SESSION['users'])){
	$_SESSION['users'] = array();
}

$_SESSION['users'][$id] = $user;


header('location: index.php');
