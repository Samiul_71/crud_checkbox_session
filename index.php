<?php
error_reporting(0);
	require_once('lib/app.php');
	$users = array();
	$users = $_SESSION['users'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>users</title>
</head>
<body>

<nav>
	<li><a href="create.php">create new user</a></li>
</nav>
<h1>Users</h1>
<table border="1">
	<thead>
		<tr>
			<th>SI</th>
			<th>Name</th>
			<th>Hobbies</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	      $SI=1;
	      if(count($users)){ 
		  foreach($users as $key =>$user){?>
		<tr>
			<td><?php echo $SI++ ?></td>
			<td><?php echo $user['full_name'] ?></td>
			<td><?php echo implode(',',$user['hobbies']) ?></td>
			<td>
				<a href="view.php?id=<?php echo $key ?>">view</a> 
				<a href="edit.php?id=<?php echo $key ?>">Edit</a>
                <a href="delete.php?id=<?php echo $key ?>"> delete</a> 
			</td>
		</tr>
		<?php }
		?>
		<tr><td colspan="4">No data found</td></tr>
		<?php } ?>
	</tbody>
</table>
</body>
</html>